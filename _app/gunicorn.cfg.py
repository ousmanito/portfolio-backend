workers = 3
max_requests = 1000
max_requests_jitter = 50

log_file = "-"
accesslog = "-"
access_log_format = '[gunicorn-access-log] "%(r)s" %(s)s %(b)s'
